import React, { Component } from 'react';

class RegisterPage extends Component {

    componentDidMount() {
        const token = localStorage.getItem("access_token");
        if (token) {
          this.props.history.push("/");
        }
      }
    

    render() {
        return (
            <div>
                Register
            </div>
        );
    }
}

export default RegisterPage;