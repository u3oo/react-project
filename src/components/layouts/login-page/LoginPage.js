import React, { Component } from "react";


export default class LoginPage extends Component {
  state = {
    login: "",
    password: ""
  };

  componentDidMount() {
    const token = localStorage.getItem("access_token");
    if (token) {
      this.props.history.push("/");
    }
  }

  onInputChange = (event, name) => {
    const newValue = {};
    newValue[name] = event.target.value;
    this.setState({ ...newValue });
  };

  onSubmit = event => {
    event.preventDefault();
    const { login, password } = this.state;
    localStorage.setItem("access_token", `${login}:${password}`);
    this.props.history.push("/");
  };

  render() {
    return (
      <div>
        <form onSubmit={this.onSubmit} className="Login__form">
          <input
            onChange={event => this.onInputChange(event, "login")}
            type="text"
          />
          <input
            onChange={event => this.onInputChange(event, "password")}
            type="password"
          />
          <button type="submit">Login</button>
        </form>
      </div>
    );
  }
}