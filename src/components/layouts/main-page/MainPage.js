import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";

import Header from "../../pages/header/Header";
import Home from "../../pages/home/Home";
import Card from "../../pages/card/Card";

export default class MainPage extends Component {
  
  componentDidMount() {
    const token = localStorage.getItem("access_token");
    if (!token) {
      this.props.history.push("/auth");
    }
  }

  render() {
    return (
      <div>
        <Header />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/card" component={Card} />
        </Switch>
      </div>
    );
  }
}
