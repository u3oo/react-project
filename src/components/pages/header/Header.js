import React from "react";
import { NavLink } from "react-router-dom";


export default function Header() {
  return (
    <div className="Header">
      <NavLink exact className="Header__link" to="/">
      |  Home | 
      </NavLink>
      <NavLink className="Header__link" to="/card">
        | Products |
      </NavLink>
    </div>
  );
}