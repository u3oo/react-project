import React, { Component } from 'react';
import ProductCard from '../product-card/ProductCard';


 class Card extends Component {
     
    render() {
        
const cardData = [
    { name: 'Blender', description: 'Buy this product, because its the best goods', price: 149, id: 1},
    { name: 'Toster', description: 'Buy this product, because its the best goods', price: 249, id: 2},
    { name: 'Teapot', description: 'Buy this product, because its the best goods', price: 128, id: 3},
    { name: 'Kazan', description: 'Buy this product, because its the best goods', price: 348, id: 4}
];
        return (
            <div className='cart container'>
                <div className='cart-info container'>
                    <h1>Cards</h1>
                    <div className="cart-w">
                        {cardData.map(product => <ProductCard key={product.id} {...product} />)}
                    </div>
                </div>
            </div>
        );
    }
}
export default Card;
