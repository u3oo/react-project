import React from 'react';
import './ProductCard.scss';

export default function ProductCard( {name, price, description}){
    
    return ( 
        <div className='product-card-container'>
            <div>
                <h2 className='product-card label'>{name}</h2>
                <p className='product-card label'>{description}</p>
                <em className='product-card label'>${price}</em>
                <button className='product-card'> add basket </button>
            </div>
        </div>
    );
};

