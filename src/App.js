import React from 'react';
import { Route, Switch } from "react-router-dom";
import NotFound from './components/pages/not-found/NotFound';
import MainPage from './components/layouts/main-page/MainPage';
import LoginPage from './components/layouts/login-page/LoginPage';
import RegisterPage from './components/layouts/register-page/RegisterPage';
import Card from './components/pages/card/Card';

function App() {
  return (
    <div className="App">
      <Switch>
        <Route exact path="/auth" component={LoginPage}/>
        <Route exact path="/register" component={RegisterPage}/>
        <Route exact path="/" component={MainPage} />
        <Route exact path="/card" component={Card} />

        <Route path="*" component={NotFound} />
      </Switch>
    </div>
  );
}

export default App;
